-- CREACIÓN DE TABLA EN EL ESQUEMA CTLG
CREATE TABLE IF NOT EXISTS exchange_rates
(
	id IDENTITY,
	buyrate DECIMAL(7, 3) NOT NULL,
	sellrate DECIMAL(7, 3) NOT NULL,
	date VARCHAR(10) NOT NULL
);
INSERT INTO exchange_rates(buyrate,sellrate,date)
VALUES(4,22,'26/03/2021');
INSERT INTO exchange_rates(buyrate,sellrate,date)
VALUES(3.14,3.5,'26/03/2021');