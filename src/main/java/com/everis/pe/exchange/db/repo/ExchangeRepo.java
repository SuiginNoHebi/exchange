package com.everis.pe.exchange.db.repo;

import java.util.List;
import org.springframework.data.repository.reactive.RxJava2CrudRepository;
import com.everis.pe.exchange.db.entity.ExchangeRates;
import io.reactivex.Flowable;

public interface ExchangeRepo extends RxJava2CrudRepository<ExchangeRates, Long>{
  Flowable<ExchangeRates> findByDateContaining(String date);
}
