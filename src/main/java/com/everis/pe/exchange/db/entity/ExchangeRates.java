package com.everis.pe.exchange.db.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class ExchangeRates {
  @Id
  private Long id;
  @Column(value = "buyrate")
  private double buyRate;
  @Column(value = "sellrate")
  private double sellRate;
  private String date;
}
