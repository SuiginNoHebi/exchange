package com.everis.pe.exchange.config;

import org.springdoc.core.SpringDocUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;
import io.reactivex.Flowable;
import io.reactivex.Single;

@Configuration
public class OpenApiConfig implements InitializingBean{

  @Override
  public void afterPropertiesSet() throws Exception {
    // TODO Auto-generated method stub
    SpringDocUtils.getConfig().addResponseWrapperToIgnore(Single.class).addResponseWrapperToIgnore(Flowable.class);
    
  }

}
