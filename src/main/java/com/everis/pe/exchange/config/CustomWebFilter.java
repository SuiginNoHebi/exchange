package com.everis.pe.exchange.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;


@Lazy
@Configuration
public class CustomWebFilter implements WebFilter {

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
    long startTime = System.currentTimeMillis();
    exchange.getResponse().beforeCommit(() -> {
      return Mono.subscriberContext().doOnNext(ctx -> {
        long totalTime = System.currentTimeMillis() - startTime;
        exchange.getResponse().getHeaders().add("X-Duration", String.valueOf(totalTime));
        System.out.println(totalTime);
      }).then();
    });
    return chain.filter(exchange);
  }
}
