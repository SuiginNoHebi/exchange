package com.everis.pe.exchange.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;


@Configuration
public class JacksonConfig {
  @Primary
  @Bean
  public ObjectMapper customObjectMapper() {
      return new Jackson2ObjectMapperBuilder()
               .modules(new JavaTimeModule(),new Jdk8Module())
               .dateFormat(new StdDateFormat())
               .failOnUnknownProperties(true)
              .serializationInclusion(JsonInclude.Include.NON_NULL)
              .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
              .createXmlMapper(false)
              .build();
  }
}
