package com.everis.pe.exchange.bussines.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.everis.pe.exchange.bussines.ExchangeBuilder;
import com.everis.pe.exchange.bussines.ExchangeProcessor;
import com.everis.pe.exchange.bussines.ExchangeSender;
import com.everis.pe.exchange.bussines.ExchangeService;
import com.everis.pe.exchange.model.PostRequest;
import com.everis.pe.exchange.model.PostRequestProfile;
import com.everis.pe.exchange.model.PostResponse;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;

@Service
@AllArgsConstructor
@CircuitBreaker(name = "exchange-default")
public class ExchangeServiceImpl implements ExchangeService {
  @Autowired
  private ExchangeBuilder builder;
  @Autowired
  private ExchangeProcessor procressor;
  @Autowired
  private ExchangeSender sender;

  @Override
  public Single<ResponseEntity<Void>> saveExchangeDB(PostRequest request) {
    // TODO Auto-generated method stub
    return sender.insertExchangeDB(builder.generateDBObject(request))
        .map(procressor::processInsert);
  }

  @Override
  public Single<PostResponse> generateAverage(PostRequestProfile profile, String date) {
    // TODO Auto-generated method stub
    return sender.listByDate(date).map(ldto -> procressor.processListAverage(profile, ldto));
  }

}
