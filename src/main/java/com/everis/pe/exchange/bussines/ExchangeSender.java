package com.everis.pe.exchange.bussines;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import com.everis.pe.exchange.db.entity.ExchangeRates;
import com.everis.pe.exchange.db.repo.ExchangeRepo;
import com.everis.pe.exchange.event.ExchangeEvenPublisher;
import com.everis.pe.exchange.event.ExchangeEvent;
import com.everis.pe.exchange.model.dto.ExchangeRatesDTO;
import io.r2dbc.spi.ConnectionFactory;
import io.reactivex.Flowable;
import io.reactivex.Single;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

@Component
public class ExchangeSender {
  @Autowired
  private ExchangeRepo exchangeRepo;
  @Autowired
  private ExchangeEvenPublisher pub;
  @Autowired
  private ApplicationEventPublisher publisher;
  
  public Single<ExchangeRates> insertExchangeDB(ExchangeRates rates) {
    return exchangeRepo.save(rates).doOnSuccess(inst -> publishExchangeEvent(convertIntoDTO(inst)));
  }
  
  public Single<List<ExchangeRatesDTO>> listByDate(String date){
    return exchangeRepo.findByDateContaining(date).collect(ArrayList<ExchangeRatesDTO>::new,(l,r)->
      l.add(convertIntoDTO(r))
    );
  }

  private ExchangeRatesDTO convertIntoDTO(ExchangeRates rate) {
    ExchangeRatesDTO dto = new ExchangeRatesDTO();
    dto.setBuyRate(Double.valueOf(rate.getBuyRate()));
    dto.setSellRate(Double.valueOf(rate.getSellRate()));
    dto.setDate(rate.getDate());
    return dto;
  }
  
//  public Flowable<ExchangeRatesDTO> findAllList() {
//    return Flowable.fromPublisher(connectionFactory.create())
//  .flatMap(connection -> connection
//      .createStatement("SELECT BUYRATE,SELLRATE,DATE FROM exchange_rates")
//      .execute())
//    .flatMap(result -> result
//      .map(this::convertIntoDTO));
//  }
//  private ExchangeRatesDTO convertIntoDTO(Row row,RowMetadata met) {
//    ExchangeRatesDTO dto = new ExchangeRatesDTO();
//    dto.setBuyRate(Double.parseDouble(row.get("BUYRATE").toString()));
//    dto.setSellRate(Double.parseDouble(row.get("SELLRATE").toString()));
//    dto.setDate(row.get("DATE").toString());
//    return dto;
//  }
  
  private final void publishExchangeEvent(ExchangeRatesDTO item) {
    this.publisher.publishEvent(new ExchangeEvent(item));
}
}
