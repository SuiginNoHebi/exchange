package com.everis.pe.exchange.bussines;

import java.util.Comparator;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.everis.pe.exchange.db.entity.ExchangeRates;
import com.everis.pe.exchange.event.ExchangeEvent;
import com.everis.pe.exchange.model.PostRequestProfile;
import com.everis.pe.exchange.model.PostResponse;
import com.everis.pe.exchange.model.dto.ExchangeRatesDTO;
import io.reactivex.Single;

@Component
public class ExchangeProcessor {

  public ResponseEntity<Void> processInsert(ExchangeRates col) throws Exception{
    if(col != null) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }else {
      throw new Exception();
    }
  }
  
  public PostResponse processListAverage(PostRequestProfile profile, List<ExchangeRatesDTO> ldto) {
    PostResponse response = new PostResponse();
    switch(profile) {
      case LOW:
        response = generatePostResponse(ldto,"LOW");
        break;
      case MEDIUM:
        response = generatePostResponse(ldto,"MEDIUM");
        response.setBuyRate(Double.valueOf(response.getBuyRate().doubleValue()/ldto.size()));
        response.setSellRate(Double.valueOf(response.getSellRate().doubleValue()/ldto.size()));
        break;
      case HIGH:
        response = generatePostResponse(ldto,"HIGH");
        break;
    }
   return response;
  }
  
  private PostResponse generatePostResponse(List<ExchangeRatesDTO> ldto,String profile) {
    double buyRate = 0.0;
    double sellRate = 0.0;
    for (ExchangeRatesDTO dto : ldto) {
      if(profile.equals("LOW")) {
        buyRate = buyRate == 0.0? dto.getBuyRate():buyRate;
        sellRate = sellRate ==0.0? dto.getBuyRate():sellRate;
        buyRate = Double.min(buyRate, dto.getBuyRate().doubleValue());
        sellRate = Double.min(sellRate, dto.getSellRate().doubleValue());
      }
      if (profile.equals("MEDIUM")) {
        buyRate = buyRate+dto.getBuyRate().doubleValue();
        sellRate = sellRate+dto.getSellRate().doubleValue();
      }
      
      if (profile.equals("HIGH")) {
        buyRate = buyRate == 0.0? dto.getBuyRate():buyRate;
        sellRate = sellRate ==0.0? dto.getBuyRate():sellRate;
        buyRate = Double.max(buyRate, dto.getBuyRate().doubleValue());
        sellRate = Double.max(sellRate, dto.getSellRate().doubleValue());
      }
    }
    PostResponse resp = new PostResponse();
    resp.setBuyRate(Double.valueOf(buyRate));
    resp.setSellRate(Double.valueOf(sellRate));
    return resp;
    
  }
}
