package com.everis.pe.exchange.bussines;

import org.springframework.stereotype.Component;
import com.everis.pe.exchange.db.entity.ExchangeRates;
import com.everis.pe.exchange.model.PostRequest;

@Component
public class ExchangeBuilder {

  public ExchangeRates generateDBObject(PostRequest request) {
    ExchangeRates obj = new ExchangeRates();
    obj.setBuyRate(request.getBuyRate());
    obj.setSellRate(request.getSellRate());
    obj.setDate(request.getDate());
    return obj;
  }
}
