package com.everis.pe.exchange.bussines;

import org.springframework.http.ResponseEntity;
import com.everis.pe.exchange.event.ExchangeEvenPublisher;
import com.everis.pe.exchange.model.PostRequest;
import com.everis.pe.exchange.model.PostRequestProfile;
import com.everis.pe.exchange.model.PostResponse;
import io.reactivex.Flowable;
import io.reactivex.Single;
import reactor.core.publisher.Flux;

public interface ExchangeService {
  public Single<ResponseEntity<Void>> saveExchangeDB(PostRequest request);
  public Single<PostResponse> generateAverage(PostRequestProfile profile,String date);
}
