package com.everis.pe.exchange.controller;

import java.time.Duration;
import java.util.Random;
import java.util.concurrent.TimeUnit;import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.everis.pe.exchange.bussines.ExchangeService;
import com.everis.pe.exchange.config.Constants;
import com.everis.pe.exchange.event.ExchangeEvenPublisher;
import com.everis.pe.exchange.event.ExchangeEvenPublisherFlow;
import com.everis.pe.exchange.event.ExchangeEvent;
import com.everis.pe.exchange.model.PostRequest;
import com.everis.pe.exchange.model.PostRequestDate;
import com.everis.pe.exchange.model.PostRequestProfile;
import com.everis.pe.exchange.model.PostResponse;
import com.everis.pe.exchange.model.dto.ExchangeRatesDTO;
import io.reactivex.BackpressureStrategy;
//import com.everis.pe.exchange.model.PostRequest;
//import com.everis.pe.exchange.model.PostRequestProfile;
//import com.everis.pe.exchange.model.PostResponse;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@RefreshScope
@RestController
@OpenAPIDefinition(info = @Info(title = "API Challenge",version = "0.0.1"),servers = @Server(url = "/exchange"))
@Slf4j
public class ExchangeController {
  private final ExchangeService service;
  
  private final Flux<ExchangeEvent> event;
  
  private final Flowable<ExchangeEvent> eventFlow;
  
  @Autowired
  public ExchangeController (ExchangeService service,ExchangeEvenPublisher pub,ExchangeEvenPublisherFlow pubF){
    this.service = service;
    this.event = Flux.create(pub).share();
    this.eventFlow = Flowable.create(pubF,BackpressureStrategy.BUFFER).subscribeOn(Schedulers.computation());
    
  }
  @Operation(tags = "Exhange insert", method = "POST",
      summary = "Permite el ingreso de intercambio de monedas")
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public Single<ResponseEntity<Void>> insetExchange(@RequestHeader HttpHeaders header,@Valid @RequestBody PostRequest postRequest) {
    return service.saveExchangeDB(postRequest).subscribeOn(Schedulers.io());
    
  }
  @Operation(tags = "Exhange profile", method = "GET",
      summary = "Permite calcular el valor minimo/promedio/maximo de los intercambio de uan fecha determinada")
  @GetMapping(value = "/{profile}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Single<PostResponse> getAverageProfile(@PathVariable("profile") PostRequestProfile profile,@Valid PostRequestDate date) {
    return service.generateAverage(profile, date.getDate());
    
  }
  @Operation(tags = "Exhange Real Time", method = "G",
      summary = "Permite Obtener todos los intercambios en la base de datos")
  @GetMapping(value="/realtime", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  public Flux<PostResponse> liveStreamExchange(){
//    Flowable<PostResponse> myFlow = service.liveExchangeList();
//    myFlow.subscribeOn(Schedulers.computation()).observeOn(Schedulers.computation()).publish().connect();
    return  this.event.map(e ->{
      ExchangeRatesDTO dto = (ExchangeRatesDTO) e.getSource();
      PostResponse resp = new PostResponse();
      resp.setBuyRate(dto.getBuyRate());
      resp.setSellRate(dto.getSellRate());
      return resp;
    });
  }
  
  @Operation(tags = "Exhange Real Time", method = "G",
      summary = "Permite Obtener todos los intercambios en la base de datos")
  @GetMapping(value="/realtimeF", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
  public Flowable<PostResponse> liveStreamExchangeF(){
//    Flowable<PostResponse> myFlow = service.liveExchangeList();
//    myFlow.subscribeOn(Schedulers.computation()).observeOn(Schedulers.computation()).publish().connect();
    return  this.eventFlow.map(e ->{
      ExchangeRatesDTO dto = (ExchangeRatesDTO) e.getSource();
      PostResponse resp = new PostResponse();
      resp.setBuyRate(dto.getBuyRate());
      resp.setSellRate(dto.getSellRate());
      return resp;
    });
  }
}
