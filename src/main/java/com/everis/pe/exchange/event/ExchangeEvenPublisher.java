package com.everis.pe.exchange.event;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import reactor.core.publisher.FluxSink;

@Component
public class ExchangeEvenPublisher implements
ApplicationListener<ExchangeEvent>,
Consumer<FluxSink<ExchangeEvent>> {

private final Executor executor;
private final BlockingQueue<ExchangeEvent> queue;

ExchangeEvenPublisher() {
    this.executor = Executors.newSingleThreadExecutor();
    this.queue = new LinkedBlockingQueue<>();
}

@Override
public void onApplicationEvent(ExchangeEvent event) {
    this.queue.offer(event);
}

@Override
public void accept(FluxSink<ExchangeEvent> sink) {
    this.executor.execute(() -> {
        while (true) {
            try {
              ExchangeEvent event = queue.take();
                sink.next(event);
            } catch (Exception e) {
                ReflectionUtils.rethrowRuntimeException(e);
            }
        }
    });
}
}
