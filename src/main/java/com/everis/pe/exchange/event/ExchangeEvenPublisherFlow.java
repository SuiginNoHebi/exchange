package com.everis.pe.exchange.event;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import reactor.core.publisher.FluxSink;

@Component
public class ExchangeEvenPublisherFlow implements
ApplicationListener<ExchangeEvent>,
FlowableOnSubscribe<ExchangeEvent> {

private final Executor executor;
private final BlockingQueue<ExchangeEvent> queue;

ExchangeEvenPublisherFlow() {
    this.executor = Executors.newSingleThreadExecutor();
    this.queue = new LinkedBlockingQueue<>();
}

@Override
public void onApplicationEvent(ExchangeEvent event) {
    this.queue.offer(event);
}

@Override
public void subscribe(FlowableEmitter<ExchangeEvent> emitter) throws Exception {
  // TODO Auto-generated method stub
    this.executor.execute(() -> {
      while (true) {
          try {
            ExchangeEvent event = queue.take();
            emitter.onNext(event);
          } catch (Exception e) {
              ReflectionUtils.rethrowRuntimeException(e);
          }
      }
  });

}
}
