package com.everis.pe.exchange.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "profiles")
public enum PostRequestProfile {
  LOW,MEDIUM,HIGH
}
