package com.everis.pe.exchange.model;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.springdoc.api.annotations.ParameterObject;
import com.everis.pe.exchange.config.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ParameterObject
public class PostRequestDate {
  @Pattern(regexp = Constants.REGEX_DATE)
  @NotEmpty
  @NotNull
  @Valid
  private String date;
}
