package com.everis.pe.exchange.model;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import com.everis.pe.exchange.config.Constants;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema(description = "Request del tipo de cambio")
public class PostRequest {
  @NotNull
  @Valid
  private Double buyRate;
  @NotNull
  @Valid
  private Double sellRate;
  @Pattern(regexp = Constants.REGEX_DATE)
  @NotEmpty
  @NotNull
  @NotBlank
  @Valid
  private String date;
}
