package com.everis.pe.exchange.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostResponse {
  private Double buyRate;
  private Double sellRate;
}
