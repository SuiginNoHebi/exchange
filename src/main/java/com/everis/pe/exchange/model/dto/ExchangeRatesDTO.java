package com.everis.pe.exchange.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExchangeRatesDTO {
  private Double buyRate;
  private Double sellRate;
  private String date;
}
