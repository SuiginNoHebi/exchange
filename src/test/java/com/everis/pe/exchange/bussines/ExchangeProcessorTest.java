package com.everis.pe.exchange.bussines;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import com.everis.pe.exchange.db.entity.ExchangeRates;
import com.everis.pe.exchange.event.ExchangeEvent;
import com.everis.pe.exchange.model.PostRequestProfile;
import com.everis.pe.exchange.model.PostResponse;
import com.everis.pe.exchange.model.dto.ExchangeRatesDTO;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeProcessorTest {

  @InjectMocks
  private ExchangeProcessor process;
  
  private List<ExchangeRatesDTO> ldto;
  @Before
  public void setUp() {
    ldto = new ArrayList<ExchangeRatesDTO>();
    ExchangeRatesDTO ob = new ExchangeRatesDTO();
    ob.setBuyRate(3.14);
    ob.setSellRate(3.14);
    ob.setDate("23/03/2021");
    ExchangeRatesDTO ob2 = new ExchangeRatesDTO();
    ob2.setBuyRate(3.15);
    ob2.setSellRate(3.15);
    ob2.setDate("23/03/2021");
    ExchangeRatesDTO ob3 = new ExchangeRatesDTO();
    ob3.setBuyRate(3.16);
    ob3.setSellRate(3.16);
    ob3.setDate("23/03/2021");
    ldto.add(ob);
    ldto.add(ob2);
    ldto.add(ob3);
  }
  @Test
  public void processInsertTestS() throws Exception {
    ExchangeRates col = new ExchangeRates();
    ResponseEntity<Void> resp = process.processInsert(col);
    assertNotNull(resp);
  }
  
  @Test
  public void processInsertTestF() {
    ExchangeRates col = null;
    ResponseEntity<Void> resp;
    try {
      resp = process.processInsert(col);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      assertTrue(e instanceof Exception);
    }
  }
  
  @Test
  public void processListAverageL() throws Exception {
    PostResponse resp = process.processListAverage(PostRequestProfile.LOW, ldto);
    assertTrue(resp.getBuyRate().equals(ldto.get(0).getBuyRate()));
    assertTrue(resp.getSellRate().equals(ldto.get(0).getSellRate()));
  }
  
  @Test
  public void processListAverageM() throws Exception {
    double sumB= 0;
    double sumS = 0;
    for (ExchangeRatesDTO dto : ldto) {
      sumB = sumB+dto.getBuyRate().doubleValue();
      sumS = sumS+dto.getSellRate().doubleValue();
    }
    PostResponse resp = process.processListAverage(PostRequestProfile.MEDIUM, ldto);
    assertTrue(resp.getBuyRate().equals(Double.valueOf(sumB/ldto.size())));
    assertTrue(resp.getSellRate().equals(Double.valueOf(sumS/ldto.size())));
  }
  
  @Test
  public void processListAverageH() throws Exception {
    PostResponse resp = process.processListAverage(PostRequestProfile.HIGH, ldto);
    assertTrue(resp.getBuyRate().equals(ldto.get(2).getBuyRate()));
    assertTrue(resp.getSellRate().equals(ldto.get(2).getSellRate()));
  }
}
