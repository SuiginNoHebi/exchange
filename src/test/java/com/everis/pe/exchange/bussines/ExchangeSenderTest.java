package com.everis.pe.exchange.bussines;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.mock.env.MockEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import com.everis.pe.exchange.db.entity.ExchangeRates;
import com.everis.pe.exchange.db.repo.ExchangeRepo;
import com.everis.pe.exchange.event.ExchangeEvenPublisher;
import com.everis.pe.exchange.event.ExchangeEvent;
import com.everis.pe.exchange.model.dto.ExchangeRatesDTO;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;
import reactor.test.publisher.TestPublisher;

@RunWith(MockitoJUnitRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ExchangeSenderTest {

  @Mock
  private ExchangeRepo repo;
  @Mock
  private ExchangeEvenPublisher pub;
  @Mock
  private ApplicationEventPublisher publisher;
  @Captor
  private ArgumentCaptor<ExchangeEvent> captor;
  @InjectMocks
  private ExchangeSender sender;
  
  @Before
  public void setUp() {
    MockEnvironment mockEnvironment = new MockEnvironment();
    ConfigurableApplicationContext context = mock(ConfigurableApplicationContext.class);
    context.setEnvironment(mockEnvironment);
  }
  
  @Test
  public void insertExchangeDBTest() {
    ExchangeRates rates = new ExchangeRates();
    rates.setBuyRate(3.14);
    rates.setSellRate(3.13);
    rates.setDate("23/03/2021");
    rates.setId(1L);
    rates.toString();
    when(repo.save(rates)).thenReturn(Single.just(new ExchangeRates()));
    TestObserver<ExchangeRates> resp = sender.insertExchangeDB(rates).test();
    resp.awaitTerminalEvent();
    resp.assertSubscribed();
    resp.assertComplete();
    resp.assertNoErrors();
  }
  
  @Test
  public void listByDateTest() {
   ExchangeRates ob = new ExchangeRates();
   ob.setBuyRate(3.14);
   ob.setSellRate(3.13);
   ob.setDate("23/03/2021");
   ob.setId(1L);
    when(repo.findByDateContaining(any(String.class))).thenReturn(Flowable.just(ob));
    TestObserver<List<ExchangeRatesDTO>> resp = sender.listByDate("").test();
    resp.awaitTerminalEvent();
    resp.assertSubscribed();
    resp.assertComplete();
    resp.assertNoErrors();
    resp.assertValue(value -> !value.isEmpty());
    resp.assertValue(value -> value.size() == 1);
    resp.assertValue(value -> value.get(0).getBuyRate() == 3.14);
    resp.assertValue(value -> value.get(0).getSellRate() == 3.13);
    resp.assertValue(value -> value.get(0).getDate().equals("23/03/2021"));
  }
  
}
