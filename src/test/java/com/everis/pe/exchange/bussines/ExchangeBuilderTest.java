package com.everis.pe.exchange.bussines;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import com.everis.pe.exchange.db.entity.ExchangeRates;
import com.everis.pe.exchange.model.PostRequest;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeBuilderTest {

  @InjectMocks
  private ExchangeBuilder builder;
  
  @Test
  public void generateDBObjectTest() {
    PostRequest request = new PostRequest();
    request.setBuyRate(3.14);
    request.setSellRate(3.14);
    request.setDate("23/03/2021");
    ExchangeRates req = builder.generateDBObject(request);
    assertTrue(req.getBuyRate() == request.getBuyRate().doubleValue());
    assertTrue(req.getSellRate() == request.getSellRate().doubleValue());
  }
}
