package com.everis.pe.exchange.bussines.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.everis.pe.exchange.bussines.ExchangeBuilder;
import com.everis.pe.exchange.bussines.ExchangeProcessor;
import com.everis.pe.exchange.bussines.ExchangeSender;
import com.everis.pe.exchange.db.entity.ExchangeRates;
import com.everis.pe.exchange.event.ExchangeEvent;
import com.everis.pe.exchange.model.PostRequest;
import com.everis.pe.exchange.model.PostRequestProfile;
import com.everis.pe.exchange.model.PostResponse;
import com.everis.pe.exchange.model.dto.ExchangeRatesDTO;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class ExchangeServiceImplTest {
  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private ExchangeBuilder builder;
  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private ExchangeProcessor procressor;
  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private ExchangeSender sender;
  @InjectMocks
  private ExchangeServiceImpl imp;
  @Before
  public void setUp() {
    imp = new ExchangeServiceImpl(builder, procressor, sender);
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void saveExchangeDBTest() throws Exception {
    PostRequest request = new PostRequest();
    ExchangeRates reqEx = new ExchangeRates();
    when(builder.generateDBObject(request)).thenReturn(reqEx);
    when(sender.insertExchangeDB(reqEx)).thenReturn(Single.just(reqEx));
    when(procressor.processInsert(reqEx)).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    TestObserver<ResponseEntity<Void>> resp = imp.saveExchangeDB(request).test();
    resp.awaitTerminalEvent();
    resp.assertSubscribed();
    resp.assertComplete();
    resp.assertNoErrors();
  }
  
  @Test
  public void generateAverageTest() {
    
    ExchangeRates reqEx = new ExchangeRates();
    PostResponse procResp = new PostResponse();
    List<ExchangeRatesDTO> lresDTO = new ArrayList<ExchangeRatesDTO>();
    when(sender.listByDate(any(String.class))).thenReturn(Single.just(lresDTO));
    when(procressor.processListAverage(PostRequestProfile.LOW,lresDTO)).thenReturn(procResp);
    TestObserver<PostResponse> resp = imp.generateAverage(PostRequestProfile.LOW, "").test();
    resp.awaitTerminalEvent();
    resp.assertSubscribed();
    resp.assertComplete();
    resp.assertNoErrors();
  }
}
