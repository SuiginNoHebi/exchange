package com.everis.pe.exchange.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import com.everis.pe.exchange.bussines.ExchangeBuilder;
import com.everis.pe.exchange.bussines.ExchangeProcessor;
import com.everis.pe.exchange.bussines.ExchangeSender;
import com.everis.pe.exchange.bussines.ExchangeService;
import com.everis.pe.exchange.bussines.impl.ExchangeServiceImpl;
import com.everis.pe.exchange.db.entity.ExchangeRates;
import com.everis.pe.exchange.db.repo.ExchangeRepo;
import com.everis.pe.exchange.event.ExchangeEvenPublisher;
import com.everis.pe.exchange.event.ExchangeEvenPublisherFlow;
import com.everis.pe.exchange.event.ExchangeEvent;
import com.everis.pe.exchange.model.PostRequest;
import com.everis.pe.exchange.model.PostRequestDate;
import com.everis.pe.exchange.model.PostRequestProfile;
import com.everis.pe.exchange.model.PostResponse;
import com.everis.pe.exchange.model.dto.ExchangeRatesDTO;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeControllerIntegrationTest {
  @Spy
  private ExchangeService service;
  @Spy
  private ExchangeBuilder builder;
  @Spy
  private ExchangeProcessor procressor;
  @Spy
  private ExchangeEvenPublisher pub;
  @Spy
  private ExchangeEvenPublisherFlow pubF;
  @Spy
  private ApplicationEventPublisher publisher;
  @Spy
  @InjectMocks
  private ExchangeSender sender;
  @Mock
  private ExchangeRepo repo;  
  @InjectMocks
  private ExchangeController controller;
  private List<ExchangeRates> ldto;
  private PostRequestDate date = new PostRequestDate();
  private double sumB= 0;
  private double sumS = 0;
  @Before
  public void setUp() {
    date.setDate("23/03/2021");
    service = new ExchangeServiceImpl(builder, procressor, sender);
    controller = new ExchangeController(service,pub,pubF);
    ldto = new ArrayList<ExchangeRates>();
    ExchangeRates ob = new ExchangeRates();
    ob.setId(1L);
    ob.setBuyRate(3.14);
    ob.setSellRate(3.14);
    ob.setDate("23/03/2021");
    ExchangeRates ob2 = new ExchangeRates();
    ob2.setBuyRate(3.15);
    ob2.setSellRate(3.15);
    ob2.setDate("23/03/2021");
    ExchangeRates ob3 = new ExchangeRates();
    ob3.setBuyRate(3.16);
    ob3.setSellRate(3.16);
    ob3.setDate("23/03/2021");
    ldto.add(ob);
    ldto.add(ob2);
    ldto.add(ob3);
    when(repo.save(any(ExchangeRates.class))).thenReturn(Single.just(ob));
    when(repo.findByDateContaining(any(String.class))).thenReturn(Flowable.fromIterable(ldto));
    
  }
  
  @Test
  public void testInsert() {
    HttpHeaders header = new HttpHeaders();
    PostRequest postRequest = new PostRequest();
    postRequest.setBuyRate(3.14);
    postRequest.setSellRate(3.14);
    postRequest.setDate("23/03/2021");
    TestObserver<ResponseEntity<Void>> resp = controller.insetExchange(header, postRequest).test();
    resp.awaitTerminalEvent();
    resp.assertSubscribed();
    resp.assertComplete();
    resp.assertNoErrors();
  }

  @Test
  public void getAverageProfileL() {
    TestObserver<PostResponse> resp = controller.getAverageProfile(PostRequestProfile.LOW,date).test();
    resp.awaitTerminalEvent();
    resp.assertSubscribed();
    resp.assertComplete();
    resp.assertNoErrors();
    resp.assertValue(value -> value != null);
    ldto.get(0).toString();
    resp.assertValue(value -> value.getBuyRate().equals(ldto.get(0).getBuyRate()));
    resp.assertValue(value -> value.getSellRate().equals(ldto.get(0).getSellRate()));
  }
  
  @Test
  public void getAverageProfileM() {

    for (ExchangeRates dto : ldto) {
      sumB = sumB+dto.getBuyRate();
      sumS = sumS+dto.getSellRate();
    }
    TestObserver<PostResponse> resp = controller.getAverageProfile(PostRequestProfile.MEDIUM, date).test();
    resp.awaitTerminalEvent();
    resp.assertSubscribed();
    resp.assertComplete();
    resp.assertNoErrors();
    resp.assertValue(value -> value.getBuyRate().equals(Double.valueOf(sumB/ldto.size())));
    resp.assertValue(value -> value.getSellRate().equals(Double.valueOf(sumS/ldto.size())));
  }
  
  @Test
  public void getAverageProfileH() {
    TestObserver<PostResponse> resp = controller.getAverageProfile(PostRequestProfile.HIGH, date).test();
    resp.awaitTerminalEvent();
    resp.assertSubscribed();
    resp.assertComplete();
    resp.assertNoErrors();
    resp.assertValue(value -> value != null);
    resp.assertValue(value -> value.getBuyRate().equals(ldto.get(2).getBuyRate()));
    resp.assertValue(value -> value.getSellRate().equals(ldto.get(2).getSellRate()));
  }
  @Test
  public void findAllStream() {
    HttpHeaders header = new HttpHeaders();
    PostRequest postRequest = new PostRequest();
    postRequest.setBuyRate(3.14);
    postRequest.setSellRate(3.14);
    postRequest.setDate("23/03/2021");
    this.publisher.publishEvent(new ExchangeEvent(new ExchangeRatesDTO()));
    TestObserver<ResponseEntity<Void>> resp = controller.insetExchange(header, postRequest).test();
    Flux<PostResponse> respS = controller.liveStreamExchange();
//    TestSubscriber<PostResponse> resp = controller.liveStreamExchange().test();
//    resp.awaitTerminalEvent(2, TimeUnit.SECONDS);
//    resp.assertSubscribed();
//    resp.assertNoErrors();
  }
}
