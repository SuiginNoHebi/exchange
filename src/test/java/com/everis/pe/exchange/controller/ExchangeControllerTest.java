package com.everis.pe.exchange.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.everis.pe.exchange.bussines.ExchangeService;
import com.everis.pe.exchange.db.entity.ExchangeRates;
import com.everis.pe.exchange.event.ExchangeEvenPublisher;
import com.everis.pe.exchange.event.ExchangeEvenPublisherFlow;
import com.everis.pe.exchange.model.PostRequest;
import com.everis.pe.exchange.model.PostRequestDate;
import com.everis.pe.exchange.model.PostRequestProfile;
import com.everis.pe.exchange.model.PostResponse;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;

@RunWith(MockitoJUnitRunner.class)
public class ExchangeControllerTest {
  @Mock
  private ExchangeService service;
  @Spy
  private ExchangeEvenPublisher pub;
  @Spy
  private ExchangeEvenPublisherFlow pubF;
  @InjectMocks
  private ExchangeController controller;
  @Before
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }
  @Test
  public void testInsert() {
    HttpHeaders header = new HttpHeaders();
    PostRequest postRequest = new PostRequest();
    controller = new ExchangeController(service,pub,pubF);
    when(service.saveExchangeDB(postRequest)).thenReturn(Single.just(new ResponseEntity<>(HttpStatus.NO_CONTENT)));
    TestObserver<ResponseEntity<Void>> resp = controller.insetExchange(header, postRequest).test();
    resp.awaitTerminalEvent();
    resp.assertSubscribed();
    resp.assertComplete();
    resp.assertNoErrors();
  }
  
  @Test
  public void getAverageProfileL() {
    PostRequestDate date = new PostRequestDate();
    date.setDate("23/03/2021");
    PostResponse respT = new PostResponse();
    respT.setBuyRate(3.14);
    respT.setSellRate(3.14);
    controller = new ExchangeController(service,pub,pubF);
    when(service.generateAverage(PostRequestProfile.LOW, "23/03/2021")).thenReturn(Single.just(respT));
    TestObserver<PostResponse> resp = controller.getAverageProfile(PostRequestProfile.LOW,date).test();
    resp.awaitTerminalEvent();
    resp.assertSubscribed();
    resp.assertComplete();
    resp.assertNoErrors();
    resp.assertValue(value -> value != null);
    resp.assertValue(value -> value.getBuyRate().equals(respT.getBuyRate()));
    resp.assertValue(value -> value.getSellRate().equals(respT.getSellRate()));
  }
  
  @Test
  public void findAllStream() {
//    List<PostResponse> ldto = new ArrayList<PostResponse>();
//    PostResponse ob = new PostResponse();
//    ob.setBuyRate(3.14);
//    ob.setSellRate(3.14);
//    PostResponse ob2 = new PostResponse();
//    ob2.setBuyRate(3.15);
//    ob2.setSellRate(3.15);
//    PostResponse ob3 = new PostResponse();
//    ob3.setBuyRate(3.16);
//    ob3.setSellRate(3.16);
//    ldto.add(ob);
//    ldto.add(ob2);
//    ldto.add(ob3);
//    when(service.liveExchangeList()).thenReturn(Flowable.fromIterable(ldto));
//    TestSubscriber<PostResponse> resp = controller.liveStreamExchange().test();
//    resp.awaitTerminalEvent(2, TimeUnit.SECONDS);
//    resp.assertSubscribed();
//    resp.assertNoErrors();
  }
}
